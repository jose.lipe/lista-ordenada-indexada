
public class LinkedList<T extends Comparable<T>> {
	private Node<T> head;
	private Node<T> tail;
	private boolean index;
	
	public LinkedList() {
		this.head = null;
		this.tail = null;
		this.index = false;
	}
	
	/**
	 * Método responsável por consultar dentro da lista
	 * 
	 * @param value
	 * 
	 * @return T
	 */
	public T search(T value) {
		
		if (this.index == false) {
			this.createIndex();
			this.index = true;
		}
		
		if (value.compareTo(this.head.getData()) == 0) {
			return this.head.getData();
		}
		
		if (value.compareTo(this.tail.getData()) > 0) {
			return null;
		}
		
		Node<T> current = this.head.getIndex();
		
		while(current != null) {
			if(value.compareTo(current.getData()) == 0) {
				return current.getData();
			}
			
			if(value.compareTo(current.getData()) < 0) {
				Node<T> prev = current.getPrevious();
				
				while(prev != null) {
					if (value.compareTo(prev.getData()) == 0) {
						return prev.getData();
					}
					
					if (value.compareTo(prev.getData()) > 0) {
						return null;
					}
					
					prev = prev.getPrevious();
				}
			}
			
			current = current.getIndex();
		}
		
		return null;
	}
	
	/**
	 * Método responsável pela criação de indices de consulta
	 * 
	 * @return void
	 */
	private void createIndex() {
		Node<T> current = this.head;
		
		while(current != null) {
			Node<T> prox = current;
			
			for (int i = 1; i < 10; i++) {
				prox = prox.getNext();
				
				if(prox.getNext() == null) {
					break;
				}
			}

			current.setIndex(prox);

			if (prox.getNext() == null) {
				current = null;
			} else {
				current = prox;
			}
		}
	}
	
	/**
	 * Método responsável por inserir na lista de forma ordenada
	 * 
	 * @param value
	 * 
	 * @return boolean
	 */
	public boolean insert(T value) {
		Node node = new Node(value);
		
		if (this.empty()) {
			this.head = this.tail = node;
			return true;
		}
		
		if (value.compareTo(this.tail.getData()) > 0) {
			this.insertTail(value, node);
			return true;
		}
		
		if (value.compareTo(this.head.getData()) < 0){
			this.insertHead(value, node);
			return true;
		}
		
		this.insertMiddle(value, node);
		
		this.index = false;
		
		return true;
	}
	
	/**
	 * Método que insere no inicio da lista
	 * 
	 * @param value
	 * @param node
	 * 
	 * @return void
	 */
	private void insertHead(T value, Node node) {
		this.head.setPrevious(node);
		node.setNext(this.head);
		this.head = node;
	}
	
	/**
	 * Método que insere no final da lista
	 * 
	 * @param value
	 * @param node
	 * 
	 * @return void
	 */
	private void insertTail(T value, Node node) {
		this.tail.setNext(node);
		node.setPrevious(this.tail);
		this.tail = node;
	}
	
	/**
	 * Metodo que insere no meio da lista
	 * 
	 * @param value
	 * @param node
	 * 
	 * @return void
	 */
	private void insertMiddle(T value, Node node) {
		Node<T> prox = this.head;
		
		while (prox != null) {
			if (value.compareTo(prox.getData()) < 0) {
				node.setPrevious(prox.getPrevious());
				node.setNext(prox);
				prox.getPrevious().setNext(node);
				prox.setPrevious(node);
				prox = null;
				continue;
			}
			
			prox = prox.getNext();
		}
	}
	
	/**
	 * Método responsável por remover elementos da lista
	 * 
	 * @param value
	 * 
	 * @return boolean
	 */
	public boolean remove(T value) {
		
		if (this.empty()) {
			return false;
		}
		
		if (value.compareTo(this.head.getData()) == 0) {
			this.removeHead(value);
			return true;
		}
		
		if (value.compareTo(this.tail.getData()) == 0) {
			this.removeTail(value);
			return true;
		}
		
		this.removeMiddle(value);
		
		this.index = false;
		
		return true;
	}
	
	/**
	 * Método que remove um elemento no inicio da lista
	 * 
	 * @param value
	 * 
	 * @return void
	 */
	private void removeHead(T value) {
		this.head = this.head.getNext();
		this.head.setPrevious(null);
	}
	
	/**
	 * Método que remove um elemento no final da lista
	 * 
	 * @param value
	 * 
	 * @return void
	 */
	private void removeTail(T value) {
		this.tail = this.tail.getPrevious();
		this.tail.setNext(null);
	}
	
	/**
	 * Método que remove no meio da lista
	 * 
	 * @param value
	 * 
	 * @return void
	 */
	private void removeMiddle(T value) {
		Node<T> prox = this.head;
		
		while (prox != null) {
			if (value.compareTo(prox.getData()) == 0) {
				prox.getPrevious().setNext(prox.getNext());
				prox.getNext().setPrevious(prox.getPrevious());
				prox = null;
				continue;
			}
			
			prox = prox.getNext();
		}
	}
	
	/**
	 * Método que informa que se a lista está vazia
	 * 
	 * @return boolean
	 */
	public boolean empty() {
		if (this.head == null) {
			return true;
		}
		
		return false;
	}
}
