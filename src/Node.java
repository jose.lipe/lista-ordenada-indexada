
public class Node<T extends Comparable<T>> {
	private T data;
	private Node<T> next;
	private Node<T> previous;
	private Node<T> index;
	
	public Node(T data) {
		this.data = data;
		this.next = null;
		this.previous = null;
		this.index = null;
	}

	public Node<T> getIndex() {
		return index;
	}

	public void setIndex(Node<T> index) {
		this.index = index;
	}

	public T getData() {
		return data;
	}

	public Node<T> getNext() {
		return next;
	}

	public void setNext(Node<T> next) {
		this.next = next;
	}

	public Node<T> getPrevious() {
		return previous;
	}

	public void setPrevious(Node<T> previous) {
		this.previous = previous;
	}
}
